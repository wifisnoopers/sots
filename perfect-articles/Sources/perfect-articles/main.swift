import PerfectLib
import PerfectHTTP
import PerfectHTTPServer
import PostgresStORM

// Setup database connection
PostgresConnector.host = "localhost"
PostgresConnector.username = "postgres"
PostgresConnector.password = "mysecretpassword"
PostgresConnector.port = 54320
PostgresConnector.database = "perfect"

// Register your own routes and handlers
var routes = Routes()
routes.add(method: .get, uri: "/") {
    request, response in
    response.setHeader(.contentType, value: "text/html")
    response.appendBody(string: "<html><title>Hello, world!</title><body>Hello, world!</body></html>")
        .completed()
}

let server = HTTPServer()
server.serverAddress = "localhost"
server.serverPort = 8080
server.addRoutes(makeRoutes())

do {
    try server.start()
} catch PerfectError.networkError(let err, let msg) {
    print("Network error: \(err) \(msg)")
}
