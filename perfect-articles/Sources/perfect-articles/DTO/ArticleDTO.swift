//
//  ArticleDTO.swift
//  perfect-articles
//
//  Created by Stein Carsten Sinke on 11/09/2019.
//

import Foundation

struct ArticleDTO: Codable {
    let title: String
    let description: String
}
