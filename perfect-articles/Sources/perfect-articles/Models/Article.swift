//
//  Article.swift
//  perfect-articles
//
//  Created by Stein Carsten Sinke on 11/09/2019.
//

import Foundation
import PostgresStORM

class Article: PostgresStORM, Codable {
    var id: Int?
    let title: String
    let description: String
    let timestamp: Date
    
    convenience override init() {
        self.init(id: nil, title: "", description: "", timestamp: Date())
    }
    
    init(id: Int?, title: String, description: String, timestamp: Date) {
        self.id = id;
        self.title = title
        self.description = description
        self.timestamp = timestamp
    }
    
    override open func table() -> String {
        return "article"
    }
}
