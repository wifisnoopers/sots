//
// Created by Stein Carsten Sinke on 10/09/2019.
//

import Foundation
import PerfectHTTP

func makeRoutes() -> Routes {
    var routes = Routes()

    routes.add(method: .get, uri: "/articles", handler: getAllArticles)
    routes.add(method: .post, uri: "/articles", handler: createArticle)

    return routes
}

func getAllArticles(request: HTTPRequest, response: HTTPResponse) {
    let article = Article(id: nil, title: "testing", description: "testing description", timestamp: Date())
    
    do {
        let newArticle = Article()
        let articles = try newArticle.
        
        print(articles)
        
        
        try response.setBody(json: article)
    } catch let error {
        print(error)
    }
    
    response.completed()
}

func createArticle(request: HTTPRequest, response: HTTPResponse) {
    do {
        let articleDTO = try request.decode(ArticleDTO.self)
        let article = Article(id: nil, title: articleDTO.title, description: articleDTO.description, timestamp: Date())
        try article.save { id in article.id = id as? Int}
        
        try response.setBody(json: article)
    } catch let error {
        print(error)
    }
    
    response.completed()
}
