import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(perfect_articlesTests.allTests),
    ]
}
#endif
