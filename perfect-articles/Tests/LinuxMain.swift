import XCTest

import perfect_articlesTests

var tests = [XCTestCaseEntry]()
tests += perfect_articlesTests.allTests()
XCTMain(tests)
