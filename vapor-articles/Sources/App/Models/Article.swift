//
//  Article.swift
//  App
//
//  Created by Stein Carsten Sinke on 09/09/2019.
//
import FluentPostgreSQL
import Vapor

struct Article: PostgreSQLModel {
    var id: Int?
    
    var title: String
    var description: String
    var timestamp: Date?
}

extension Article: PostgreSQLMigration {}

extension Article: Content {}

extension Article: Parameter {}
