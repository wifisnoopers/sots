//
//  ArticleDTO.swift
//  App
//
//  Created by Stein Carsten Sinke on 09/09/2019.
//

import Foundation

struct ArticleDTO: Codable {
    let title: String
    let description: String
    
    func toArticle() -> Article {
        return Article(id: nil, title: self.title, description: self.description, timestamp: Date())
    }
}

extension ArticleDTO {
    
}
