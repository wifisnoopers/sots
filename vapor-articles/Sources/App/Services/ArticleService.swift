//
//  ArticleService.swift
//  App
//
//  Created by Stein Carsten Sinke on 09/09/2019.
//

import Vapor

struct ArticleService {
    func getAll(_ req: Request) throws -> Future<[Article]> {
        let articles = Article.query(on: req).all()
        return articles
    }
    
    func create(_ req: Request, article: Article) -> Future<Article> {
        let article = article.create(on: req)
        return article
    }
    
    func get(_ req: Request, id: Int) -> Future<Article?> {
        let article = Article.find(id, on: req)
        return article
    }
}

extension ArticleService: Service {}
