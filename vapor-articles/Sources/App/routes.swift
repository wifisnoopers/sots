import Vapor

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    let articleController = ArticleController()
    router.get("articles", use: articleController.getAll)
    router.get("articles", Int.parameter, use: articleController.getById)
    router.post("articles", use: articleController.post)

}
