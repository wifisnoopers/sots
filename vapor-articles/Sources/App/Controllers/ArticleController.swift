//
//  ArticleController.swift
//  App
//
//  Created by Stein Carsten Sinke on 09/09/2019.
//

import Vapor

struct ArticleController {
    func getAll(_ req: Request) throws -> Future<[Article]> {
        let articleService = try req.make(ArticleService.self)
        return try articleService.getAll(req)
    }
    
    func post(_ req: Request) throws -> Future<Article> {
        let articleService = try req.make(ArticleService.self)
        let articleDTO = try req.content.decode(ArticleDTO.self)
        
        let article = articleDTO.flatMap(to: Article.self) { article in
            let article = Article(id: nil, title: article.title, description: article.description, timestamp: Date())
            return articleService.create(req, article: article)
        }
        
        return article
    }
    
    func getById(_ req: Request) throws -> Future<Article> {
        let id = try req.parameters.next(Int.self)
        
        let articleService = try req.make(ArticleService.self)
        
        return articleService.get(req, id: id).thenThrowing() { article in
            guard let article = article else {
                throw Abort(HTTPResponseStatus.notFound)
            }
            return article
        }
    }
}
