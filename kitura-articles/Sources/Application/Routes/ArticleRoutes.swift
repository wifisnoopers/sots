//
//  ArticleRoutes.swift
//  Application
//
//  Created by Stein Carsten Sinke on 10/09/2019.
//
import Foundation
import KituraContracts

func initializeArticleRoutes(app: App) {
    app.router.get("/articles", handler: app.getAllArticles)
    app.router.get("/articles", handler: app.getArticleById)
    app.router.post("/articles", handler: app.createArticle)
}

extension App {
    func getAllArticles(completion: @escaping ([Article]?, RequestError?) -> Void) {
        Article.findAll(completion)
    }
    
    func getArticleById(id: Int, completion: @escaping (Article?, RequestError?) -> Void) {
        Article.find(id: id, completion)
    }
    
    func createArticle(articleDTO: ArticleDTO, completion: @escaping (Article?, RequestError?) -> Void) {
        let article = Article(title: articleDTO.title, description: articleDTO.description, timestamp: Date())
        
        article.save { article, error in
            print(error ?? "")
            completion(article, error)
        }
        
    }
}
