import Foundation
import Kitura
import LoggerAPI
import Configuration
import CloudEnvironment
import KituraContracts
import Health
import KituraOpenAPI
import SwiftKueryORM
import SwiftKueryPostgreSQL

public let projectPath = ConfigurationManager.BasePath.project.path
public let health = Health()

public class App {
    let router = Router()
    let cloudEnv = CloudEnv()
    
    private func DBSetup() {
        let pool = PostgreSQLConnection.createPool(
        host: "localhost",
        port: 54320,
        options: [
        .databaseName("kitura"),
        .password("mysecretpassword"),
        .userName("postgres"),
        ],
        poolOptions: ConnectionPoolOptions(initialCapacity: 10)
        )
        Database.default = Database(pool)
    }
    
    private func DBMigration() {
        do {
            try Article.createTableSync()
        } catch let e {
            print(e)
        }
    }

    public init() throws {
        // Run the metrics initializer
        initializeMetrics(router: router)
        
    }

    func postInit() throws {
        // Endpoints
        initializeHealthRoutes(app: self)
        initializeArticleRoutes(app: self)
        KituraOpenAPI.addEndpoints(to: router)
        DBSetup()
        DBMigration()
    }

    public func run() throws {
        try postInit()
        Kitura.addHTTPServer(onPort: cloudEnv.port, with: router)
        Kitura.run()
    }
}
