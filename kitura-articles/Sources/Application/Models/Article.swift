//
//  Article.swift
//  Application
//
//  Created by Stein Carsten Sinke on 10/09/2019.
//
import Foundation
import SwiftKueryORM
import SwiftKueryPostgreSQL

struct Article: Codable {   
    let title: String
    let description: String
    let timestamp: Date
}

extension Article: Model { }
